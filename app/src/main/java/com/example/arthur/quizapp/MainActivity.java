package com.example.arthur.quizapp;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

public class MainActivity extends AppCompatActivity {
    Bundle br,bp;
    String perguntas[] = new String[5];
    int respostas [] = new int[5];

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);



    }
    public void Jogar(View view){
        bp = new Bundle();
        br = new Bundle();

        perguntas[0] = "1 + 1?";
        perguntas[1] = "2 + 2";
        perguntas[2] = "3 + 3";
        perguntas[3] = "4 + 4";
        perguntas[4] = "5 + 5";

        respostas[0] = 2 ;
        respostas[1] = 4;
        respostas[2] = 3;
        respostas[3] = 8;
        respostas[4] = 10;

        //teste
        Intent it = new Intent(this, PrimeiraPergunta.class);


        bp.putStringArray("perguntas",perguntas);
        br.putIntArray("respostas",respostas);

        it.putExtra("perguntas",bp);
        it.putExtra("respostas",br);

        startActivity(it);
        finish();
    }

}

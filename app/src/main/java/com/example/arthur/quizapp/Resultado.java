package com.example.arthur.quizapp;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

public class Resultado extends AppCompatActivity {
    Bundle bp,br,pont;
    TextView tv;
    String [] perguntas;
    RadioGroup radioGroup;
    RadioButton radioButton;

    int [] respostas;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_resultado);
        Intent it = getIntent();
        bp = new Bundle();
        br = new Bundle();
        pont = new Bundle();

        bp = it.getBundleExtra("perguntas");
        br = it.getBundleExtra("respostas");
        pont = it.getBundleExtra("pontuacao");

       tv = (TextView) findViewById(R.id.res);

        perguntas = bp.getStringArray("perguntas");
        respostas = br.getIntArray("respostas");

        String resultado = String.valueOf(pont.getInt("pontuacao"));
        tv.setText(resultado);
    }
}

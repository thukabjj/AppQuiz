package com.example.arthur.quizapp;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

public class TerceiraPergunta extends AppCompatActivity {
    Bundle bp,br,pont;
    TextView tv;
    String [] perguntas;
    RadioGroup radioGroup;
    RadioButton radioButton;

    int [] respostas;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_terceira_pergunta);

        Intent it = getIntent();
        bp = new Bundle();
        br = new Bundle();
        pont = new Bundle();

        bp = it.getBundleExtra("perguntas");
        br = it.getBundleExtra("respostas");
        pont = it.getBundleExtra("pontuacao");

        tv = (TextView) findViewById(R.id.p3);

        perguntas = bp.getStringArray("perguntas");
        respostas = br.getIntArray("respostas");

        tv.setText(perguntas[2]);
    }
    public void ProximaPergunta(View view){
        Intent it = new Intent(this,QuartaPergunta.class);

        radioGroup = (RadioGroup) findViewById(R.id.rg3);

        int idSelecionado = radioGroup.getCheckedRadioButtonId();

        radioButton = (RadioButton) findViewById(idSelecionado);
        int res = Integer.parseInt(radioButton.getText().toString());
        int pontos = 0;
        pontos = pont.getInt("pontuacao");

        if(res == respostas[2]){

            pont.putInt("pontuacao",++pontos);

        }

        bp.putStringArray("perguntas",perguntas);
        br.putIntArray("respostas",respostas);

        it.putExtra("perguntas",bp);
        it.putExtra("respostas",br);
        it.putExtra("pontuacao",pont);

        startActivity(it);
        finish();
    }
}

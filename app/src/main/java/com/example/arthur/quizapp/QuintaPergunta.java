package com.example.arthur.quizapp;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

public class QuintaPergunta extends AppCompatActivity {
    Bundle bp,br,pont;
    TextView tv;
    String [] perguntas;
    RadioGroup radioGroup;
    RadioButton radioButton;

    int [] respostas;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_quinta_pergunta);

        Intent it = getIntent();
        bp = new Bundle();
        br = new Bundle();
        pont = new Bundle();

        bp = it.getBundleExtra("perguntas");
        br = it.getBundleExtra("respostas");
        pont = it.getBundleExtra("pontuacao");

        tv = (TextView) findViewById(R.id.p5);

        perguntas = bp.getStringArray("perguntas");
        respostas = br.getIntArray("respostas");

        tv.setText(perguntas[4]);
    }
    public void ProximaPergunta(View view){
        int idSelecionado = 0;
        Intent it = new Intent(this,Resultado.class);

        radioGroup = (RadioGroup) findViewById(R.id.rg5);

        idSelecionado = radioGroup.getCheckedRadioButtonId();

        radioButton = (RadioButton) findViewById(idSelecionado);
        int res = Integer.parseInt(radioButton.getText().toString());
        int pontos = 0;
        pontos = pont.getInt("pontuacao");

        if(res == respostas[4]){

            pont.putInt("pontuacao",++pontos);

        }

        bp.putStringArray("perguntas",perguntas);
        br.putIntArray("respostas",respostas);

        it.putExtra("perguntas",bp);
        it.putExtra("respostas",br);
        it.putExtra("pontuacao",pont);

        startActivity(it);
        finish();
    }
}
